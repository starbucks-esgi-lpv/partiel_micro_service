import {IsMongoId, IsString, MaxLength, MinLength} from "class-validator";
import {Expose} from "class-transformer";

export class SessionCreateDto {

    @MinLength(32)
    @MaxLength(32)
    @IsString()
    @Expose()
    token: string;

    @MinLength(1)
    @IsString()
    @Expose()
    platform: string;

    @IsMongoId()
    @Expose()
    account: string;
}