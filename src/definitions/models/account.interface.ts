export interface IAccount {
    _id: any; //ObjectID
    login: string;
    password: string;
    createdDate: Date;
    updatedDate: Date;
    firstName: string;
    lastName: string;
    birthDate?: Date;
}
