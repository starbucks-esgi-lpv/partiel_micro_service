import {IAccount} from "./account.interface";

export interface ISession {
    _id: any;
    token: string;
    platform: string;
    account: string | IAccount;
}
