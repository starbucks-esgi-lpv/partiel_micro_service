import 'reflect-metadata'; // OBLIGATOIRE POUR LES ANNOTATIONS
import {config} from "dotenv";
config(); //permet de charger le fichier .env

import {MongooseUtils, IAccountDocument, AccountSchema, ServiceRegistry} from "./mongoose";
import {startServer} from "./express";

async function main() {
    const connection = await MongooseUtils.connect();
    const serviceRegistry = new ServiceRegistry(connection);

    startServer(serviceRegistry);
}

main().catch(console.error);
