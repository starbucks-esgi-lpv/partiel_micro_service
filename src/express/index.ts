import * as express from "express";
import {AccountController} from "./controllers";
import {ServiceRegistry} from "../mongoose";

export function startServer(serviceRegistry: ServiceRegistry): void {
    const app = express();
    app.use(express.json()); // permet de parser le body http en JSON

    const accountController = new AccountController(serviceRegistry);
    app.use('/account', accountController.buildRoutes()); // enregistrer l'ensemble des routes du controller

    app.listen(process.env.PORT, function (){
        console.log(`Server listening on port ${process.env.PORT}...`)
    })
}
