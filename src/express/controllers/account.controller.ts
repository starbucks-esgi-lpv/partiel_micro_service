import {BaseController} from "./base.controller";
import {Request, Response, Router} from "express";
import {MongooseUtils} from "../../mongoose";
import {AccountCreateDto, AccountLoginDto} from "../../definitions/dto";
import {plainToInstance} from "class-transformer";
import {SessionCreateDto} from "../../definitions/dto/session";
import {SecurityUtils} from "../../security.utils";
import {sessionMiddleware} from "../middlewares";

export class AccountController extends BaseController {

    async subscribe(req: Request, res: Response){
        const dto = await AccountController.createAndValidateDTO(AccountCreateDto, req.body, res);
        if (!dto){
            return
        }

        try {
            const account = await this.serviceRegistry.accountService.create(dto);
            res.status(201).json(account);
        } catch (err) {
            if (MongooseUtils.isDuplicateKeyError(err)){
                res.status(409).end(); // 409 --> conflict
            } else {
                res.status(500).end() ;
            }
        }
    }

    async login(req: Request, res: Response){
        const dto = await AccountController.createAndValidateDTO(AccountLoginDto, req.body, res);
        if (!dto){
            return;
        }

        const account = await this.serviceRegistry.accountService.getWithLogin(dto);
        if(!account){
            res.status(401).end(); //unauthorized
            return;
        }

        const sessionDTO = await AccountController.createAndValidateDTO(SessionCreateDto, {
            token: SecurityUtils.generateToken(32),
            platform: req.header('user-agent'),
            account: account._id.toString()
        }, res);
        if (!sessionDTO){
            return;
        }

        const session = await this.serviceRegistry.sessionService.create(sessionDTO);

        res.status(201).json(session);
    }

    async me(req: Request, res: Response) {
        res.json(req.account);
    }

    buildRoutes(): Router {
        const router = Router();
        router.post('/subscribe', this.subscribe.bind(this));
        router.post('/login', this.login.bind(this));
        router.get('/me', sessionMiddleware(this.serviceRegistry), this.me.bind(this));
        return router;
    }
}