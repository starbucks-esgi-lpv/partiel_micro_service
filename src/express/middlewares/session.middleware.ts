import {ServiceRegistry} from "../../mongoose";
import {NextFunction, Request, Response} from "express";
import {IAccount} from "../../definitions";

declare module 'express'{
    export interface Request{
        account?: IAccount;
    }
}

export function sessionMiddleware(registry: ServiceRegistry) {
    return async function (req: Request, res: Response, next: NextFunction) {
        const authorization = req.header('authorization');
        if (!authorization){
            res.status(401).end();
            return;
        }
        const parts = authorization.split(' '); //On découpe le champs authorization à partir d'un espace
        if (parts.length != 2 || parts[0] !== 'Bearer' || parts[1].length !== 32){ //On doit impérativement avoir 2 parties
            res.status(401).end();
            return;
        }
        const token = parts[1];
        const session = await registry.sessionService.getByToken(token);
        if (!session) {
            res.status(403).end(); //forbidden access
            return;
        }
        req.account = session.account as IAccount;
        next(); //Permet d'accer à la route.
    }
}