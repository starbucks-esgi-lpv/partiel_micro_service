import {Mongoose} from "mongoose";
import {AccountService} from "./account.service";
import {SessionService} from "./session.service";

export class ServiceRegistry {

    public readonly accountService: AccountService;
    public readonly sessionService: SessionService;
    constructor(connection: Mongoose) {
        this.accountService = new AccountService(connection);
        this.sessionService = new SessionService(connection);
    }

}