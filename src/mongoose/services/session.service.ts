import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {ISessionDocument, SessionSchema} from "../schemas";
import {SessionCreateDto} from "../../definitions/dto/session";

export class SessionService extends BaseService {

    protected model: Model<ISessionDocument>

    public constructor(connection: Mongoose) {
        super(connection);
        this.model = connection.model<ISessionDocument>('Session', SessionSchema);
    }

    public create(dto: SessionCreateDto): Promise<ISessionDocument> {
        return this.model.create(dto);
    }

    public getByToken(token: string): Promise<ISessionDocument | null> {
        return this.model.findOne({
            token
        }).populate('account').exec();
    }
}