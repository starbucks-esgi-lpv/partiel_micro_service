import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {AccountSchema, IAccountDocument} from "../schemas";
import {AccountCreateDto, AccountLoginDto} from "../../definitions/dto";

export class AccountService extends BaseService {

    protected model: Model<IAccountDocument>

    public constructor(connection: Mongoose) {
        super(connection);
        this.model = connection.model<IAccountDocument>('Account', AccountSchema);
    }

    public create(dto: AccountCreateDto): Promise<IAccountDocument> {
        return this.model.create(dto);
    }

    public getWithLogin(dto: AccountLoginDto): Promise<IAccountDocument | null> {
        return this.model.findOne({dto}).exec();
    }

}