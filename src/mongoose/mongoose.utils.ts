import * as mongoose from "mongoose";

export class MongooseUtils{

    public static async connect(): Promise<mongoose.Mongoose> {
        return mongoose.connect(process.env.MONGO_URI, {
            auth: {
                username: process.env.MONGO_USER,
                password: process.env.MONGO_PASSWORD
            },
            authSource: 'admin',
            autoCreate: true //permet de créer automatiquement les collections
        });
    }

    public static isDuplicateKeyError(err: Error): boolean {
        if (err.name == 'MongoServerError'){
            const code = err['code'];
            return code === 11_000;
        }
        return false;
    }
}
