import {Schema, SchemaTypes} from "mongoose";
import {IAccount} from "../../definitions";

export const AccountSchema = new Schema({
    login: {
        type: SchemaTypes.String,
        required: true,
        minlength: 4,
        unique: true
    },
    password: {
        type: SchemaTypes.String,
        required: true,
        minlength: 128,
        maxlength: 128
    },
    firstName: {
        type: SchemaTypes.String,
        required: true,
        minlength: 1
    },
    lastName: {
        type: SchemaTypes.String,
        required: true,
        minlength: 1
    },
    birthDate: {
        type: SchemaTypes.Date
    }
},{
    timestamps: {
        createdAt: 'createdDate',
        updatedAt: 'updatedDate'
    },
    versionKey: false,
    collection: 'accounts'
});

export type IAccountDocument = IAccount & Document;
